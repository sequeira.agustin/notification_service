package com.notification.domain;

import com.notification.Gateway;
import com.notification.domain.rule.Rule;
import com.notification.domain.timewindow.SecondTimeWindow;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class RuleTest {

    public static final Notification NOTIFICATION_PANAFONIC_NEWS = new Notification("panafonic", "news");

    @Test
    void testIsOverRangeReturnsFalse_WhenIsTheFirstNotificationSend() {
        // @Given
        final Rule rule = Rule.newInstanceOf(new SecondTimeWindow(2), NOTIFICATION_PANAFONIC_NEWS, 1, mock(Gateway.class));
        final Email email = mock(Email.class);
        when(email.getCreation()).thenReturn(LocalDateTime.of(2020, 2, 15, 6, 0, 0));
        // @When
        final boolean isOverRange = rule.isOverRange(email);
        // @Then
        assertThat(isOverRange).isFalse();
    }

    @Test
    void testIsOverRangeReturnsTrueWhenEmailCreatedT0AndT2ForA2SecondsTimeWindowsAndMaxNumberOfNotification1() {
        // @Given
        final Email firstEmail = mock(Email.class);
        final Email secondEmail = mock(Email.class);

        when(firstEmail.getCreation()).thenReturn(LocalDateTime.of(2020, 2, 15, 6, 0, 0));
        when(secondEmail.getCreation()).thenReturn(LocalDateTime.of(2020, 2, 15, 6, 0, 2));

        final Rule rule = Rule.newInstanceOf(new SecondTimeWindow(2), NOTIFICATION_PANAFONIC_NEWS, 1, mock(Gateway.class));
        rule.isOverRange(firstEmail);
        // @When
        final boolean isOverRange = rule.isOverRange(secondEmail);
        // @Then
        assertThat(isOverRange).isTrue();
    }

    @Test
    void testIsOverRangeReturnsFalseWhenEmailCreatedT0AndT3ForA2SecondsTimeWindowsAndMaxNumberOfNotification1() {
        // @Given
        final Email firstEmail = mock(Email.class);
        final Email secondEmail = mock(Email.class);

        when(firstEmail.getCreation()).thenReturn(LocalDateTime.of(2020, 2, 15, 6, 0, 0));
        when(secondEmail.getCreation()).thenReturn(LocalDateTime.of(2020, 2, 15, 6, 0, 3));

        final Rule rule = Rule.newInstanceOf(new SecondTimeWindow(2), NOTIFICATION_PANAFONIC_NEWS, 1, mock(Gateway.class));
        rule.isOverRange(firstEmail);
        // @When
        final boolean isOverTheRange = rule.isOverRange(secondEmail);
        // @Then
        assertThat(isOverTheRange).isFalse();
    }

    @Test
    void testIsOverRangeReturnsFalseWhenEmailCreatedT0AndT2ForA3SecondsTimeWindowsAndMaxNumberOfNotification3() {
        // @Given
        final Email firstEmail = mock(Email.class);
        final Email secondEmail = mock(Email.class);

        when(firstEmail.getCreation()).thenReturn(LocalDateTime.of(2020, 2, 15, 6, 0, 0));
        when(secondEmail.getCreation()).thenReturn(LocalDateTime.of(2020, 2, 15, 6, 0, 2));

        final Rule rule = Rule.newInstanceOf(new SecondTimeWindow(3), NOTIFICATION_PANAFONIC_NEWS, 3, mock(Gateway.class));
        rule.isOverRange(firstEmail);
        // @When
        final boolean isOverTheRange = rule.isOverRange(secondEmail);
        // @Then
        assertThat(isOverTheRange).isFalse();
    }

    @Test
    void testIsOverRangeReturnsFalseWhenEmailCreatedT0T2AndT3ForA3SecondsTimeWindowsAndMaxNumberOfNotification3() {
        // @Given
        final Email firstEmail = mock(Email.class);
        final Email secondEmail = mock(Email.class);
        final Email third = mock(Email.class);

        when(firstEmail.getCreation()).thenReturn(LocalDateTime.of(2020, 2, 15, 6, 0, 0));
        when(secondEmail.getCreation()).thenReturn(LocalDateTime.of(2020, 2, 15, 6, 0, 2));
        when(third.getCreation()).thenReturn(LocalDateTime.of(2020, 2, 15, 6, 0, 3));

        final Rule rule = Rule.newInstanceOf(new SecondTimeWindow(3), NOTIFICATION_PANAFONIC_NEWS, 3, mock(Gateway.class));
        rule.isOverRange(firstEmail);
        rule.isOverRange(secondEmail);
        // @When
        final boolean isOverTheRange = rule.isOverRange(third);
        // @Then
        assertThat(isOverTheRange).isFalse();
    }

    @Test
    void testIsOverRangeReturnsTrueWhenEmailCreatedT0T2T3AndT3ForA3SecondsTimeWindowsAndMaxNumberOfNotification3() {
        // @Given
        final Email firstEmail = mock(Email.class);
        final Email secondEmail = mock(Email.class);
        final Email third = mock(Email.class);
        final Email fourth = mock(Email.class);

        when(firstEmail.getCreation()).thenReturn(LocalDateTime.of(2020, 2, 15, 6, 0, 0));
        when(secondEmail.getCreation()).thenReturn(LocalDateTime.of(2020, 2, 15, 6, 0, 2));
        when(third.getCreation()).thenReturn(LocalDateTime.of(2020, 2, 15, 6, 0, 3));
        when(fourth.getCreation()).thenReturn(LocalDateTime.of(2020, 2, 15, 6, 0, 3));

        final Rule rule = Rule.newInstanceOf(new SecondTimeWindow(3), NOTIFICATION_PANAFONIC_NEWS, 3, mock(Gateway.class));
        rule.isOverRange(firstEmail);
        rule.isOverRange(secondEmail);
        rule.isOverRange(third);
        // @When
        final boolean isOverTheRange = rule.isOverRange(fourth);
        // @Then
        assertThat(isOverTheRange).isTrue();
    }
}