package com.notification.domain.rule;

import com.notification.domain.rule.Range;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.LocalDateTime;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class RangeTest {

    private final LocalDateTime startDate = LocalDateTime.of(2020, 2, 15, 6, 0, 0);
    private final Range range= new Range(startDate, 3);

    @DisplayName("Test send, returns false when mail are over range limit")
    @ParameterizedTest
    @MethodSource("provideRanges")
    void testRangeClass(Range range, LocalDateTime date, Boolean expectedResult) {
        // @Given
        // @When
        final boolean isInBetween = range.isInBetween(date);
        // @Then
        assertThat(isInBetween).isEqualTo(expectedResult);
    }

    private static Stream<Arguments> provideRanges() {
        return Stream.of(
                Arguments.of(new Range(LocalDateTime.of(2020, 2, 15, 6, 0, 0), 3), LocalDateTime.of(2020, 2, 15, 6, 0, 2), true),
                Arguments.of(new Range(LocalDateTime.of(2020, 2, 15, 6, 0, 0), 3), LocalDateTime.of(2020, 2, 15, 6, 0, 3), true),
                Arguments.of(new Range(LocalDateTime.of(2020, 2, 15, 6, 0, 0), 3), LocalDateTime.of(2020, 2, 15, 6, 0, 4), false),
                Arguments.of(new Range(LocalDateTime.of(2020, 2, 15, 6, 0, 0), 3), LocalDateTime.of(2020, 2, 15, 6, 0, 0), true),
                Arguments.of(new Range(LocalDateTime.of(2020, 2, 15, 6, 0, 0), 3), LocalDateTime.of(2020, 2, 15, 5, 59, 59), false)
        );
    }
}