package com.notification.domain;

import com.notification.domain.Email;
import com.notification.domain.MailMan;
import com.notification.domain.Notification;
import com.notification.domain.rule.Rule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MailManTest {

    private final Rule panafonicRule = mock(Rule.class);
    private final Rule soniRule = mock(Rule.class);
    private MailMan mailMan;

    @BeforeEach
    void setUp() {
        final List<Rule> rules = new LinkedList<>();

        rules.add(this.panafonicRule);
        rules.add(soniRule);

        this.mailMan = MailMan.newInstanceOfMailMan(rules);
    }

    @DisplayName("Test send, returns false when mail are over range limit")
    @ParameterizedTest
    @MethodSource("provideEmails")
    void testSendEmail(Email first, Notification firstNotification, Boolean firstIsOverRange, Email second, Notification secondNotification, Boolean secondIsOverRange, Boolean expectedValue) {
        // @Given
        when(first.getNotification()).thenReturn(firstNotification);
        when(second.getNotification()).thenReturn(firstNotification);
        when(this.panafonicRule.isOverRange(first)).thenReturn(firstIsOverRange);
        when(this.panafonicRule.isOverRange(second)).thenReturn(secondIsOverRange);
        when(this.panafonicRule.getNotification()).thenReturn(new Notification("panafonic", "news"));
        // @When
        final boolean hasSentTheEMail = this.mailMan.send(second);
        // @Then
        assertThat(hasSentTheEMail).isEqualTo(expectedValue);
    }

    private static Stream<Arguments> provideEmails() {
        return Stream.of(
                Arguments.of(mock(Email.class), new Notification("panafonic", "news"), false, mock(Email.class), new Notification("panafonic", "news"), false, true),
                Arguments.of(mock(Email.class), new Notification("panafonic", "news"), false, mock(Email.class), new Notification("panafonic", "news"), true, false)
        );
    }
}