package com.notification;

import com.notification.domain.MailMan;
import com.notification.domain.Notification;
import com.notification.domain.rule.Rule;
import com.notification.domain.timewindow.SecondTimeWindow;

import java.util.Arrays;

public class App {

    public static void main(String[] args) {

        final Gateway gateway = new Gateway();

        final Rule soniRule = Rule.newInstanceOf(new SecondTimeWindow(122334),
                Notification.builder().type("news").user("soni").build(),
                4, gateway);

        final Rule panasorni = Rule.newInstanceOf(new SecondTimeWindow(122334),
                Notification.builder().type("project").user("panasorni").build(),
                2, gateway);

        final MailMan mailMan = MailMan.newInstanceOfMailMan(Arrays.asList(soniRule, panasorni));

        mailMan.send(Notification.builder().type("news").user("panasorni").build(), "Hi!");
    }
}
