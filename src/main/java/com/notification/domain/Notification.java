package com.notification.domain;

import lombok.Builder;
import lombok.EqualsAndHashCode;

@Builder
@EqualsAndHashCode
public class Notification {
    private String user;
    private String type;
}
