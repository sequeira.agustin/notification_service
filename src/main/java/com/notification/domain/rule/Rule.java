package com.notification.domain.rule;

import com.notification.Gateway;
import com.notification.domain.Email;
import com.notification.domain.Notification;
import com.notification.domain.timewindow.TimeWindow;
import com.sun.istack.internal.NotNull;

import java.util.Objects;

public class Rule {
    private final long timeWindow;
    private final Notification notification;
    private Range range;
    private final long maxNumberOfNotifications;
    private final Gateway gateway;
    private long count;

    private Rule(long timeWindow, Notification notification, long maxNumberOfNotifications, Gateway gateway) {
        this.timeWindow = timeWindow;
        this.notification = notification;
        this.maxNumberOfNotifications = maxNumberOfNotifications;
        this.gateway = gateway;
        this.count = 0;
    }

    /**
     * Build a new instance of Rule class.
     * @param timeWindow is the period of time.
     * @param notification is a group of attributes.
     * @param maxNumberOfNotifications is the max amount of message we can send in a period of time.
     * @param gateway send mail to destination.
     * @return
     */
    public static Rule newInstanceOf(final TimeWindow timeWindow,
                                     @NotNull final Notification notification,
                                     final long maxNumberOfNotifications,
                                     Gateway gateway) {

        if (Objects.isNull(notification)) {
            throw new IllegalArgumentException("Null is not a valid value for notification parameter");
        }

        return new Rule(timeWindow.toSeconds(), notification, maxNumberOfNotifications, gateway);
    }

    public Notification getNotification() {
        return this.notification;
    }

    /**
     * Verify if the notifications are over range according to the rule definition and send email if is necessary.
     * @param email to send, not null.
     * @return true if email was sent.
     */
    public boolean isOverRange(@NotNull final Email email) {

        // is over range when no notification was allowed.
        if (this.maxNumberOfNotifications == 0) {
            return true;
        }

        // is under range when is the first notification and max number of notifications are higher than 0
        if (this.count == 0) {
            this.range = new Range(email.getCreation(), this.timeWindow);
            this.gateway.sendMail(email);
            this.count = 1;

            return false;
        }

        // the email's creation date is in between the date range and the notification count is equal or smaller than
        // max number of notification for given range.
        if (this.range.isInBetween(email.getCreation()) && this.count + 1 <= this.maxNumberOfNotifications) {
            this.count++;
            this.gateway.sendMail(email);

            return false;
        }

        // the email's creation date is in between the date range and the notification count is bigger than
        // max number of notification for given range.
        if (this.range.isInBetween(email.getCreation()) && this.count + 1 > this.maxNumberOfNotifications) {
            this.count++;

            return true;
        }

        // the email's creation date is out of the date range, we must reset the count and setup a new range.
        if (this.range.isO1utsideOf(email.getCreation())) {
            this.range = new Range(email.getCreation(), this.timeWindow);

            this.gateway.sendMail(email);
            this.count = 1;

            return false;
        }

        throw new IllegalArgumentException("Business exceptions, this scenario isn't cover by business rules.");
    }
}
