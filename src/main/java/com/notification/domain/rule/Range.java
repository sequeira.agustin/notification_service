package com.notification.domain.rule;

import java.time.LocalDateTime;

public class Range {
    private final LocalDateTime start;
    private final LocalDateTime end;

    public Range(LocalDateTime start, long seconds) {
        this.start = start;
        this.end = start.plusSeconds(seconds);
    }

    boolean isInBetween(LocalDateTime date) {
        return (this.start.isBefore(date) && date.isBefore(this.end)) || (this.start.isEqual(date) || date.isEqual(this.end));
    }

    boolean isO1utsideOf(LocalDateTime date) {
        return !this.isInBetween(date);
    }
}
