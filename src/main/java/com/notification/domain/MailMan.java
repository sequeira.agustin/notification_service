package com.notification.domain;

import com.notification.domain.rule.Rule;
import com.sun.istack.internal.NotNull;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class MailMan {

    private final List<Rule> rules;

    private MailMan(List<Rule> rules) {
        this.rules = rules;
    }

    /**
     * Build a new instance of MailMan.
     * @param rules to limit the notifications, not null.
     * @return a new instance of MailMan class.
     */
    public static MailMan newInstanceOfMailMan(@NotNull List<Rule> rules) {

        if (Objects.isNull(rules)) {
            throw new IllegalArgumentException("rule parameter can't be null");
        }

        return new MailMan(new LinkedList<>(rules));
    }

    /**
     * Send Emails.
     * @param email not null value, including notification destiny and message.
     * @return true if email were sent otherwise false.
     */
    boolean send(@NotNull final Email email) {
        return this.rules.stream()
                .filter(rule -> rule.getNotification().equals(email.getNotification()))
                .map(rule -> !rule.isOverRange(email))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Unable to Notify user, couldn't find rule for the given Email"));
    }


    public boolean send(@NotNull final Notification notification, @NotNull final String message) {

        if (Objects.isNull(notification)) {
            throw new IllegalArgumentException("notification parameter can't be null");
        }

        if (Objects.isNull(message)) {
            throw new IllegalArgumentException("\"rule parameter can't be null\"");
        }

        final Email email = Email.builder().creation(LocalDateTime.now())
                .message(message)
                .notification(notification).build();

        return this.send(email);
    }
}