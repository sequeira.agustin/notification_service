package com.notification.domain.timewindow;

public class SecondTimeWindow implements TimeWindow {

    private final long seconds;

    public SecondTimeWindow(long seconds) {
        this.seconds = seconds;
    }

    @Override
    public long toSeconds() {
        return this.seconds;
    }
}
