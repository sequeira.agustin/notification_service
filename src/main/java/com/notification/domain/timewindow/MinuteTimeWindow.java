package com.notification.domain.timewindow;

public class MinuteTimeWindow implements TimeWindow {

    private final long minutes;

    public MinuteTimeWindow(long minutes) {
        this.minutes = minutes;
    }

    @Override
    public long toSeconds() {
        return 60 * this.minutes;
    }
}
