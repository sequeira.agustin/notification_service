package com.notification.domain.timewindow;

public class HourTimeWindow implements TimeWindow {

    private final long hours;

    public HourTimeWindow(long hours) {
        this.hours = hours;
    }

    @Override
    public long toSeconds() {
        return 60 * 60 * this.hours;
    }
}
