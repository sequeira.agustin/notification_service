package com.notification.domain.timewindow;

public interface TimeWindow {
    long toSeconds();
}
