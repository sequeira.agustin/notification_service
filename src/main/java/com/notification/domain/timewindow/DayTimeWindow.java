package com.notification.domain.timewindow;

public class DayTimeWindow implements TimeWindow {

    private final long days;

    public DayTimeWindow(long days) {
        this.days = days;
    }

    @Override
    public long toSeconds() {
        return 60 * 60 * 24 * this.days;
    }
}
