package com.notification.domain;

import lombok.Builder;

import java.time.LocalDateTime;

@Builder
public class Email {
    private LocalDateTime creation;
    private Notification notification;
    private String message;

    public LocalDateTime getCreation() {
        return this.creation;
    }

    public Notification getNotification() {
        return this.notification;
    }
}